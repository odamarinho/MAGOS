#include <cstdlib>
#include "../include/maze.h"

bool Maze::destroy_wall(size_t x, size_t y, size_t side)
{
	if(maze_height <= x || maze_width <= y) return false;
	if(x < 0 || y < 0) return false;

	if(side == UP && x > 0)
	{
		cells[x][y].up.k_down = true;
		cells[x-1][y].down.k_down = true;
		return true;
	}

	else if(side == DOWN && x < maze_height-1)
	{
		cells[x][y].down.k_down = true;
		cells[x+1][y].up.k_down = true;
		return true;
	}

	else if(side == LEFT && y > 0) 
	{
		cells[x][y].left.k_down = true;
		cells[x][y-1].right.k_down = true;
		return true;
	}

	else if(side == RIGHT && y < maze_width-1)
	{
		cells[x][y].right.k_down = true;
		cells[x][y+1].left.k_down = true;
		return true;
	}

	return false;
}

bool Maze::solve_done()
{
	return solver->cell_type == EXIT;
}

bool Maze::build_done()
{
	if(paths.size() == 1) return true;

	return false;
}

void Maze::merge_paths(int n, int np)
{
	std::list<Cell*>::iterator it = paths[np].begin();

	while(it != paths[np].end())
	{
		paths[n].push_back(*it);
		std::advance(it, 1);
	}

	for(size_t i = np; i < paths.size()-1; i++)
	{
		paths[i] = paths[i+1];
	}

	paths.resize(paths.size()-1);
	//paths.shrink_to_fit();
}

int Maze::find_cell_path(Cell * c)
{
	size_t i = 0;
	while(i < paths.size())
	{
		std::list<Cell*>::iterator it = paths[i].begin();
		while(it != paths[i].end())
		{
			if(*it == c) 
			{
				return i;
				break;
			}
			std::advance(it, 1);
		}
		i++;
	}

	return -1;
}

void Maze::Build()
{
	size_t r_x = rand() % maze_height;
	size_t r_y = rand() % maze_width;

	size_t r_wall = rand() % 4;

	bool is_destroyed = false;


	if(r_wall == UP) is_destroyed = cells[r_x][r_y].up.k_down;
	else if(r_wall == DOWN) is_destroyed = cells[r_x][r_y].down.k_down;
	else if(r_wall == LEFT) is_destroyed = cells[r_x][r_y].left.k_down;
	else if(r_wall == RIGHT) is_destroyed = cells[r_x][r_y].right.k_down;

	if(is_destroyed) return;

	Cell * r_cell = &cells[r_x][r_y];
	Cell * neighbor;

	if(r_wall == UP && r_x > 0) neighbor = &cells[r_x-1][r_y];
	else if(r_wall == DOWN && r_x < maze_height-1) neighbor = &cells[r_x+1][r_y];
	else if(r_wall == LEFT && r_y > 0) neighbor = &cells[r_x][r_y-1];
	else if(r_wall == RIGHT && r_y < maze_width-1) neighbor = &cells[r_x][r_y+1];

	else return;

	
	int path = find_cell_path(r_cell);
	if(path == -1) return;


	std::list<Cell*>::iterator it = paths[path].begin();
	while(it != paths[path].end())
	{
		if(*it == neighbor) return;
		std::advance(it, 1);
	}

	int neighbor_path = find_cell_path(neighbor);

	//std::cout << r_x << ", " << r_y << "\n";

	merge_paths(path, neighbor_path);

	if(destroy_wall(r_x, r_y, r_wall)) build_step++;
}

void Maze::Solve()
{
	//std::cout << "1. AQUI PASSOU: " << solver_x << ", " << solver_y <<"\n";

	solver = &cells[solver_x][solver_y];

	//std::cout << "2. AQUI PASSOU: " << solver_x << ", " << solver_y <<"\n";


	if(right_hand_pos == LEFT)
	{
		if(solver->left.k_down)
		{
			solver = &cells[solver_x][--solver_y];
			right_hand_pos = UP;
		}

		else if(solver->down.k_down)
		{
			solver = &cells[++solver_x][solver_y];
		}

		else
		{
			right_hand_pos = DOWN;
		}
	}

	else if(right_hand_pos == DOWN)
	{
		if(solver->down.k_down)
		{
			solver = &cells[++solver_x][solver_y];
			right_hand_pos = LEFT;
		}

		else if(solver->right.k_down)
		{
			solver = &cells[solver_x][++solver_y];
		}

		else
		{
			right_hand_pos = RIGHT;
		}
	}

	else if(right_hand_pos == UP)
	{
		if(solver->up.k_down)
		{
			solver = &cells[--solver_x][solver_y];
			right_hand_pos = RIGHT;
		}

		else if(solver->left.k_down)
		{
			solver = &cells[solver_x][--solver_y];
		}

		else 
		{
			right_hand_pos = LEFT;
		}
	}

	else if(right_hand_pos == RIGHT)
	{
		if(solver->right.k_down)
		{
			solver = &cells[solver_x][++solver_y];
			right_hand_pos = DOWN;
		}

		else if(solver->up.k_down)
		{
			solver = &cells[--solver_x][solver_y];
		}

		else 
		{
			right_hand_pos = UP;
		}
	}

	if(solver->cell_type != ENTRANCE && solver->cell_type != EXIT) solver->visited = true;

	solve_step++;

}