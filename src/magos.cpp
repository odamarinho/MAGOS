#include <iostream>
#include "../include/render.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../include/stb_image_write.h"

int main(int argc, char** argv)
{
	std::cout << " >>> Bem vindo ao Maze Generator and Solver(MAGOS)!! \n\n";
	std::cout << " >>> Nós já contactamos uma equipe de anões construtores e eles estão trabalhando no seu labirinto!\n";

	size_t rows;
	size_t columns;
	size_t height;
	size_t width;

	try
	{
		rows = std::stoi(argv[1]);
		columns = std::stoi(argv[2]);
		height = std::stoi(argv[4]);
		width = std::stoi(argv[3]);
	}
	catch(std::logic_error)
	{
		std::cout << " >>> Você não entrou com dados o suficiente. Colocamos os valores padrão.\n";

		rows = 5;
		columns = 5;
		height = 600;
		width = 800;
	}

	if(rows < 2 || columns < 2)
	{
		std::cout << " >>> Os valores inseridos para as dimensões do labirinto são muito pequenos. Substituímos pelos nossos valores padrão.\n";
		rows = 5;
		columns = 5;
	}

	if(height < 100 || width < 100)
	{
		std::cout << " >>> Os valores inseridos para as dimensões da imagem são muito pequenos. Substituímos pelos nossos valores padrão.\n";
		height = 600;
		width = 800;
	}

	srand(time(NULL));

	Maze m(rows, columns);

	std::cout << " >>> Building... \n";
	while(!m.build_done())
	{
		m.Build(); 
		Render_image(m, width, height, "builder");
	}

	std::cout << " >>> Labirinto Construído com sucesso!\n\n";

	std::cout << " >>> Lançamos agora um mago na entrada do seu labirinto para que ele encontre a saída.\n";
	std::cout << " >>> Solving...\n";
	while(!m.solve_done())
	{
		m.Solve();
		Render_image(m, width, height, "solver");
	}

	std::cout << " >>> Labirinto Resolvido com sucesso!\n\n";

	return 0;
}