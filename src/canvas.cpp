/*!
 * Canvas class implementation.
 * @file canvas.cpp
 */

#include <stdexcept>
#include <iostream>

#include "../include/canvas.h"

namespace canvas {

    /*!
     * Deep copy of the canvas.
     * @param clone The object we are copying from.
     */
    Canvas::Canvas( const Canvas & clone )
    {
        m_width = clone.m_width;
        m_height = clone.m_height;
        m_pixels = clone.m_pixels;
        m_line_thikness = clone.m_line_thikness;
    }


    /*!
     * @param source The object we are copying information from.
     * @return A reference to the `this` object.
     */
    Canvas& Canvas::operator=( const Canvas & source )
    {
        m_width = source.m_width;
        m_height = source.m_height;
        m_pixels = source.m_pixels;
        m_line_thikness = source.m_line_thikness;

        return *this;
    }

    void Canvas::clear( const Color& color )
    {
        for(unsigned int i = 0; i < m_width * m_height * 3; i += 3)
        {
        	*(m_pixels + i + 0)	= color.channels[0];
        	*(m_pixels + i + 1) = color.channels[1];
        	*(m_pixels + i + 2) = color.channels[2];
        }
    }

    /*!
     * @throw `std::invalid_argument()` it the pixel coordinate is located outside the canvas.
     * @param p The 2D coordinate of the pixel we want to know the color of.
     * @return The pixel color.
     */
    Color Canvas::pixel( coord_type x, coord_type y ) const
    {
        if(x >= m_width || y >= m_height) throw std::invalid_argument("Parâmetro maior que uma das dimensões da imagem.");

        Color color;

        color.channels[0] = *(m_pixels + 3*x + (3*m_width*y) + 0);
        color.channels[1] = *(m_pixels + 3*x + (3*m_width*y) + 1);
        color.channels[2] = *(m_pixels + 3*x + (3*m_width*y) + 2);

        return color;
    }

    /*!
     * @note Nothing is done if the  pixel coordinate is located outside the canvas.
     * @param p The 2D coordinate of the pixel we want to change the color.
     * @param c The color.
     */
    void Canvas::pixel( coord_type x, coord_type y, const Color& c )
    {
        if(x >= m_width || y >= m_height) return;

        *(m_pixels + 3*x + (3*m_width*y) + 0) = c.channels[0];
        *(m_pixels + 3*x + (3*m_width*y) + 1) = c.channels[1];
        *(m_pixels + 3*x + (3*m_width*y) + 2) = c.channels[2];
    }


    /*!
     * Draws on the canvas a horizontal line.
     * @param p The 2D coordinate of the initial pixel of the line.
     * @param length The horizontal length of the line in pixels.
     */
    void Canvas::hline( coord_type x, coord_type y, size_t length, const Color& color )
    {
       	for(unsigned short largura_linha = 0; largura_linha < m_line_thikness; largura_linha++)	
       	{
       		for(size_t i = 0; i < length*3; i+=3)
        	{
        		*(m_pixels + 3*x + (3*m_width*(y + largura_linha)) + i + 0) = color.channels[0];
        		*(m_pixels + 3*x + (3*m_width*(y + largura_linha)) + i + 1) = color.channels[1];
        		*(m_pixels + 3*x + (3*m_width*(y + largura_linha)) + i + 2) = color.channels[2];
        	}
        }
    }

    /*!
     * Draws on the canvas a vertical line.
     * @param p The 2D coordinate of the initial pixel of the line.
     * @param length The vertical length of the line in pixels.
     */
    void Canvas::vline( coord_type x, coord_type y, size_t length, const Color& color )
    {
        for(unsigned short largura_linha = 0; largura_linha < m_line_thikness; largura_linha++)	
       	{
       		for(size_t i = 0; i < length*3*m_width; i+=3*m_width)
        	{
        		*(m_pixels + 3*(x + largura_linha) + (3*m_width*y) + i + 0) = color.channels[0];
        		*(m_pixels + 3*(x + largura_linha) + (3*m_width*y) + i + 1) = color.channels[1];
        		*(m_pixels + 3*(x + largura_linha) + (3*m_width*y) + i + 2) = color.channels[2];
        	}
        }
    }

    /*!
     * Draws on the canvas a filled box. The origin of the box is the top-left corner.
     * The box is compose of horizontal lines, drawn top to bottom.
     * @param p The 2D coordinate of the initial pixel of the box (top-left corner).
     * @param width The box's width in pixels.
     * @param height The box's heigth in pixels.
     */
    void Canvas::box( coord_type x, coord_type y, size_t width, size_t height , const Color& color)
    {
        unsigned short aux = m_line_thikness;
        m_line_thikness = 1;

        for(size_t i = 0; i < height; i++)
        {
        	hline(x, y+i, width, color);
        }

        m_line_thikness = aux;
    }
}

//================================[ canvas.cpp ]================================//
