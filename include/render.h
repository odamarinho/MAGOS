#include <iostream>
#include "canvas.h"
#include "maze.h"
#include "stb_image_write.h"

using namespace canvas;

class Render_image
{
	public:
	/*! Construtor: Transforma um objeto da classe Maze em uma imagem do labirinto.
	 *@param maze Labirinto a ser colocado na imagem.*/
	Render_image(Maze &maze, size_t image_width, size_t image_height, std::string work)
	{

		Canvas c(image_width, image_height);

		size_t margem_superior = 10*image_height / 100;
		size_t margem_esquerda = 10*image_width / 100;
		size_t margem_inferior = margem_superior;
		size_t margem_direita = margem_esquerda;

		coord_type start_pixel_x = margem_esquerda;
		coord_type start_pixel_y = margem_superior;

		size_t square_width = (image_width - margem_esquerda - margem_direita) / maze.get_width();
		size_t square_height = (image_height - margem_superior - margem_inferior) / maze.get_height();

		c.clear(LIGHT_GREY);
		c.thickness(3);

		c.hline(start_pixel_x, start_pixel_y, (image_width - margem_direita - margem_direita), BLACK);
		c.vline(start_pixel_x, start_pixel_y, (image_height - margem_superior - margem_inferior), BLACK);
		c.hline(start_pixel_x, (image_height - margem_superior), (image_width - margem_esquerda - margem_direita), BLACK);
		c.vline((image_width - margem_esquerda), start_pixel_y, (image_height - margem_superior - margem_inferior), BLACK);


		coord_type pixel_x = start_pixel_x;
		coord_type pixel_y = start_pixel_y;

		for(size_t i = 0; i < maze.get_height(); i++)
		{
			for(size_t j = 0; j < maze.get_width(); j++)
			{
				if(!maze.get_cell(i, j).down.k_down) c.hline(pixel_x, pixel_y + square_height, square_width, BLACK);
				if(!maze.get_cell(i, j).right.k_down) c.vline(pixel_x + square_width, pixel_y, square_height, BLACK);

				if(maze.get_cell(i, j).cell_type == ENTRANCE) 
					c.box(pixel_x + c.thickness(), pixel_y + c.thickness(), square_width - c.thickness(), square_height - c.thickness(), DEEP_SKY_BLUE);

				if(maze.get_cell(i, j).cell_type == EXIT) 
					c.box(pixel_x + c.thickness(), pixel_y + c.thickness(), square_width - c.thickness(), square_height - c.thickness(), GREEN);

				if(maze.get_cell(i, j).visited) 
					c.box(pixel_x + c.thickness(), pixel_y + c.thickness(), square_width - c.thickness(), square_height - c.thickness(), YELLOW);

				if(work == "solver" && maze.get_solver_pointer() == &maze.get_cell(i, j) && maze.get_cell(i, j).cell_type != ENTRANCE)
					c.box(pixel_x + c.thickness(), pixel_y + c.thickness(), square_width - c.thickness(), square_height - c.thickness(), RED);

				pixel_x += square_width;
			}
			pixel_y += square_height;
			pixel_x = start_pixel_x;
		}

		auto width = c.width();
    	auto height = c.height();
    	auto pixels = c.buffer();

    	
		std::string image_name = "";
    	if(work == "builder") image_name = work + "/building_" + maze.get_build_step() + ".png";
    	if(work == "solver") image_name = work + "/solving_" + maze.get_solve_step() + ".png";

    	const char* image_name_c = image_name.c_str();

		stbi_write_png_compression_level = 0;    // defaults to 8; set to higher for more compression
   		stbi_write_png( image_name_c,      // file name
                width, height,        // image dimensions
                3,                    // # of channels per pixel
                pixels,               // the pixels
                width*3)  ;           // length of a row (in bytes), see above.
	}
};