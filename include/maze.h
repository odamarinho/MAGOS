#include <iostream>
#include <vector>
#include <list>
#include "common.h"

enum CELL_TYPE { COMMON, ENTRANCE, EXIT };
enum CELL_SIDE { UP=0, DOWN=1, LEFT=2, RIGHT=3 };

typedef struct s_wall
{
	bool k_down = false;
} Wall;

typedef struct cell_s
{
	bool visited = false;
	size_t cell_type;
	Wall up;
	Wall down;
	Wall left;
	Wall right;
} Cell;

class Maze
{
	private:
	size_t maze_width;
	size_t maze_height;
	Cell ** cells;
	std::vector<std::list<Cell*>> paths;
	Cell * solver;
	size_t solver_x = 0;
	size_t solver_y = 0;
	size_t right_hand_pos = LEFT;
	int build_step = -1;
	int solve_step = -1;

	public:
	/*!Cria um Maze vazio de tamanho w por h. Caso os valores não sejam passados, será criado um Maze 2x2.
	 *@parem w Largura do Maze.
	 *@param h Altura do Maze.*/
	Maze(size_t h=2, size_t w=2)
	{
		maze_width = w;
		maze_height = h;

		cells = new Cell*[maze_height];
		for(size_t i = 0; i < maze_height; i++)
		{
			cells[i] = new Cell[maze_width];
		}

		for(size_t i = 0; i < maze_height; i++)
		{
			for(size_t j = 0; j < maze_width; j++)
			{
				cells[i][j].cell_type = COMMON;
				std::list<Cell*> single_path;
				single_path.push_back(&cells[i][j]);
				paths.push_back(single_path);
			}
		}

		cells[0][0].cell_type = ENTRANCE;
		cells[maze_height-1][maze_width-1].cell_type = EXIT;

		solver = &cells[0][0];
	}

	~Maze() { delete cells; }

	/*! Retorna a quantidade de vezes que uma parede foi destruída durante o prcesso de building.
	 *@return Uma string contendo o número de vezes que uma parede foi destruída durante o processo de Building. */
	std::string get_build_step() 
	{
		std::string str = std::to_string(build_step);
		return str; 
	}

	/*! Retorna a quantidade de vezes que um passo foi dado no processo de Solve.
	 *@return Uma string contendo o número de vezes que um passo foi dado durante o processo de Solve. */
	std::string get_solve_step()
	{
		std::string str = std::to_string(solve_step);
		return str;
	}

	/*! Função usada para pegar o apontador de Célula usado para resolver o labirinto.
	  @return O apontador qua aponta para a atual célula na resolução do labirinto.*/
	Cell * get_solver_pointer() { return solver; }

	/*! Pega a altura do labirinto.
	 *@return altura do labirinto.*/
	size_t get_height() { return maze_height; }

	/*! Pega a largura do labirinto.
	 *@return largura do labirinto.*/
	size_t get_width() { return maze_width; }

	/*! Pega uma célula do labirinto.
	 *@param x Coordenada x da céluca que será retornada*/
	Cell & get_cell(size_t x, size_t y) { return cells[x][y]; }

	
	/*! Função parar detirminar o fim da construção do labirinto. 
	 *return Quando o só existir um caminho(vec.size == 1) a função retorna true. Retorna false se não.*/
	bool build_done();

	/*! Função usada para saber quando a saída do labirinto foi encontrada.
	  @return True se o apontador de resolução aponta para uma célula marcada como EXIT. False se não.*/
	bool solve_done();

	/*! Pega a Matriz de células e usa o processo de destruir muros até formar um labirinto com somente um caminho da entrada até a saída.*/ 
	void Build();

	/*! Tenta encontrar a célula marcada como EXIT começando a partir da célula ENTRANCE. O processo usado para encontrar a saída se chama 'Right Hand Rule'.*/
	void Solve();

	/*! Encontra o 'Path' que a cécula pertence, em outras palavras em qual list ela se encontra.
	 *@param cell Célula que se quer saber o caminho.
	 @return índice da list que contém a célula no vetor de lists. Retorna -1 caso a célula passada pelo parametro esteja vazia.*/
	int find_cell_path(Cell * c);

	/*! Une dois paths em somente um e exclui o path vazio.
	  @param p índice para o primeiro path. Esse ficará com as cálulas do np.
	  @param np índice para o segundo path. As célúlas desse serão removidas para p e porfim o path indicado pelo índice np será exluido.*/
	void merge_paths(int p, int np);

	/*! Método usado para derrubar uma parede da célula. A parede correspondente na célula visinha também a destruída.
	 *@param x Coordenada x da célula que terá o wall derrubado.
	 *@param y Coordenada y da célula que terá o wall derrubado.
	 *@param side Muro que será destruído(UP, DOWN, LEFT or RIGHT).
	 *@return True se conseguiu destruir a parede. False caso a parede já tenha sido destruída ou caso as coordenadas dadas estejam fora do labirinto.*/
	bool destroy_wall(size_t x, size_t y, size_t side);
};
