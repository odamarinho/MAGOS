Maze Generator and Solver(MAGOS)

Autor: Odailton Marinho de Melo.

 O projeto MAGOS consiste em um software capaz de gerar labirintos perfeitos e resolvê-los. 
Nesse caso, a resolução de um labirinto nada mais é do que encontrar e percorrer o caminho que conecta a 
célula superior esquerda(ENTRADA) e a inferior direita(SAÍDA).
No caso desse projeto, cada passo do processo de 'Build' e 'Solve' é registrado em uma imagem .png
Build = Processo de geração do Labirinto.
Solve = Processo de resolução de um Labirinto.


COMO COMPILAR: A compilação é feita por linha de comando (Terminal). Entre no diretório do projeto e 
compile os arquivos 'canvas.cpp', 'maze.cpp' e 'magos.cpp', todos na pasta 'src'.

EXEMPLO: g++ -std=c++11 -Wall src/canvas.cpp src/maze.cpp src/magos.cpp -o magos

No exemplo acima será gerado um executável chamado 'magos'. Ao executá-lo é necessário passar como parâmetro as 
dimensões do labirinto(quantidade de linhas e colunas, respectivamente) e as dimensões da imagem em pixels(largura e altura, respectivamente).

EXEMPLO: ./magos 18 16 1200 800

O exemplo acima irá gerar um babirinto 18x16 em uma imagem de 1200x800 pixels.
Caso qualquer dimensão do labirinto passada seja menor que 2 células, o software usará as dimensões padrão de tamanho de labirinto (5x5).
Caso qualquer dimensão da imagem passada seja menor que 100 pixels, o software usará as dimensões padrão de tamanho de imagem (800x600).
As imagens de cada passo do processo de Buil podem ser vistas na pasta builder.
As imagens de cada passo do processo de Solve podem ser vistas na pasta solver.


O método de Solve utilizado foi a 'regra da mão direita'.

Na parte de Build usei listas encadeadas para representar os caminhos das células e isso ocasiounou em uma execução mais lenta.
Alguns labirintos grandes (20x20, por exemplo) ultrapassam os 5 minutos somente na criação.