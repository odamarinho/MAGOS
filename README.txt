Maze Generator and Solver(MAGOS)

Autor: Odailton Marinho de Melo.

 >> O projeto MAGOS consiste em um software capaz de gerar labirintos perfeitos e resolv�-los. 
Nesse caso, a resolu��o de um labirinto nada mais � do que encontrar e percorrer o caminho que conecta a 
c�lula superior esquerda(ENTRADA) e a inferior direita(SA�DA).
No caso desse projeto, cada passo do processo de 'Build' e 'Solve' � registrado em uma imagem .png
Build = Processo de gera��o do Labirinto.
Solve = Processo de resolu��o de um Labirinto.


 >> COMO COMPILAR: A compila��o � feita por linha de comando (Terminal). Entre no diret�rio do projeto e 
compile os arquivos 'canvas.cpp', 'maze.cpp' e 'magos.cpp', todos na pasta 'src'.
EXEMPLO: g++ -std=c++11 -Wall src/canvas.cpp src/maze.cpp src/magos.cpp -o magos

>> No exemplo acima ser� gerado um execut�vel chamado 'magos'. Ao execut�-lo � necess�rio passar como par�metro as 
dimens�es do labirinto(quantidade de linhas e colunas, respectivamente) e as dimens�es da imagem em pixels(largura e altura, respectivamente).
EXEMPLO: ./magos 18 16 1200 800

O exemplo acima ir� gerar um babirinto 18x16 em uma imagem de 1200x800 pixels.
Caso qualquer dimens�o do labirinto passada seja menor que 2 c�lulas, o software usar� as dimens�es padr�o de tamanho de labirinto (5x5).
Caso qualquer dimens�o da imagem passada seja menor que 100 pixels, o software usar� as dimens�es padr�o de tamanho de imagem (800x600).
As imagens de cada passo do processo de Buil podem ser vistas na pasta builder.
As imagens de cada passo do processo de Solve podem ser vistas na pasta solver.


>> O m�todo de Solve utilizado foi a 'regra da m�o direita'.

>> Na parte de Build usei listas encadeadas para representar os caminhos das c�lulas e isso ocasiounou em uma execu��o mais lenta.
Alguns labirintos grandes (20x20, por exemplo) ultrapassam os 5 minutos somente na cria��o.



